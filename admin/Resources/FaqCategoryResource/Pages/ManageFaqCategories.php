<?php

namespace Neon\Admin\Resources\FaqCategoryResource\Pages;

use Neon\Admin\Resources\FaqCategoryResource;
use Filament\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageFaqCategories extends ManageRecords
{
  protected static string $resource = FaqCategoryResource::class;

  protected function getHeaderActions(): array
  {
    return [
      Actions\CreateAction::make()
        ->slideOver(),
    ];
  }
}
