<?php

namespace Neon\Admin\Resources;

use Filament\Forms;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Str;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Neon\Admin\Resources\Traits\NeonAdmin;
use Neon\Admin\Resources\FaqResource\Pages;
use Neon\Admin\Resources\FaqResource\RelationManagers;
use Neon\Attributable\Models\Attribute;
use Neon\Faq\Models\Faq;
use Neon\Models\Basic;
use Neon\Models\Scopes\ActiveScope;
use Neon\Models\Statuses\BasicStatus;
use Neon\Site\Models\Scopes\SiteScope;
use Neon\Site\Models\Site;

class FaqResource extends Resource
{
  use NeonAdmin;

  protected static ?int $navigationSort = 6;

  protected static ?string $model = Faq::class;

  protected static ?string $navigationIcon = 'heroicon-o-question-mark-circle';

  protected static ?string $activeNavigationIcon = 'heroicon-s-question-mark-circle';

  protected static ?string $recordTitleAttribute = 'title';

  // public static function getNavigationParentItem(): string
  // {
  //   return __('neon-admin::admin.resources.sites.title');
  // }

  public static function getNavigationLabel(): string
  {
    return __('neon-admin::admin.navigation.faq');
  }

  public static function getNavigationGroup(): string
  {
    return __('neon-admin::admin.navigation.web');
  }

  public static function getModelLabel(): string
  {
    return __('neon-admin::admin.models.faq');
  }

  public static function getPluralModelLabel(): string
  {
    return __('neon-admin::admin.models.faqs');
  }

  public static function items(): array
  {
    $t = [
      Forms\Components\Select::make('faq_category_id')
        ->relationship(
          name: 'faq_category',
          titleAttribute: 'title',
          modifyQueryUsing: fn (Builder $query) => $query->withoutGlobalScopes()->withoutTrashed(),
        )
        ->label(trans('neon-admin::admin.resources.faq.form.fields.category.label'))
        ->searchable()
        ->required(),
      Forms\Components\TextInput::make('question')
        ->required()
        ->label( __('neon-admin::admin.resources.faq.form.fields.question.label')),
      Forms\Components\SpatieMediaLibraryFileUpload::make(Faq::MEDIA)
        ->label(trans('neon-admin::admin.resources.faq.form.fields.media.label'))
        ->collection(Faq::MEDIA)
        ->responsiveImages(),
      Forms\Components\RichEditor::make('answer')
        ->required()
        ->label(__('neon-admin::admin.resources.faq.form.fields.answer.label'))
        ->toolbarButtons([
          'bold',
          'italic',
          'bulletList',
          'orderedList',
          'blockquote',
          'redo',
          'strike',
          'underline',
          'undo',
        ]),
      Forms\Components\Section::make()
        ->columns([
          'sm' => 3,
          'xl' => 6,
          '2xl' => 9,
        ])
        ->schema([
          Forms\Components\Select::make('status')
            ->label(__('neon-admin::admin.resources.faq.form.fields.status.label'))
            ->required()
            ->reactive()
            ->native(false)
            ->default(BasicStatus::default())
            ->options(BasicStatus::class)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ]),
          Forms\Components\DateTimePicker::make('published_at')
            ->label(trans('neon-admin::admin.resources.faq.form.fields.published_at.label'))
            ->native(false)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ]),
          Forms\Components\DateTimePicker::make('expired_at')
            ->label(trans('neon-admin::admin.resources.faq.form.fields.expired_at.label'))
            ->minDate(now())
            ->native(false)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ])
        ])

    ];

    return $t;
  }

  public static function table(Table $table): Table
  {
    return $table
      ->columns([
        Tables\Columns\TextColumn::make('faq_category.title')
          ->label(__('neon-admin::admin.resources.faq.form.fields.category.label'))
          ->state(
            static function (\Illuminate\Database\Eloquent\Model $record): string {
              return $record->faq_category()->withoutGlobalScopes()->first()->title;
            })
          ->searchable(),
        Tables\Columns\SpatieMediaLibraryImageColumn::make(Faq::MEDIA)
          ->toggleable(isToggledHiddenByDefault: true),
        Tables\Columns\TextColumn::make('question')
          ->label(__('neon-admin::admin.resources.faq.form.fields.question.label'))
          ->searchable(),
        Tables\Columns\TextColumn::make('answer')
          ->label(__('neon-admin::admin.resources.faq.form.fields.answer.label'))
          ->html()
          ->searchable(),
        Tables\Columns\IconColumn::make('status')
          ->label(__('neon-admin::admin.resources.faq.form.fields.status.label'))
          ->icon(fn (BasicStatus $state): string => match ($state) {
            BasicStatus::New      => 'heroicon-o-sparkles',
            BasicStatus::Active   => 'heroicon-o-check-circle',
            BasicStatus::Inactive => 'heroicon-o-x-circle',
          })
          ->color(fn (BasicStatus $state): string => match ($state) {
            BasicStatus::New      => 'gray',
            BasicStatus::Active   => 'success',
            BasicStatus::Inactive => 'danger',
          })
          ->searchable()
          ->sortable(),
        Tables\Columns\TextColumn::make('published_at')
          ->label(__('neon-admin::admin.resources.faq.form.fields.published_at.label'))
          ->description(fn (Faq $record): string => $record->expired_at?->format('M j, Y') ?: '-')
          ->date()
          ->sortable(),
        
        Tables\Columns\TextColumn::make('created_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
        Tables\Columns\TextColumn::make('updated_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
        Tables\Columns\TextColumn::make('deleted_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
      ])
      ->filters([
        Tables\Filters\SelectFilter::make('faq_category')
          ->label(__('neon-admin::admin.resources.faq.form.fields.category.label'))
          ->relationship('faq_category', 'title', fn (Builder $query) => $query->withoutGlobalScopes()),
        Tables\Filters\SelectFilter::make('status')
          ->options(BasicStatus::class)
          ->label('Státusz')
          ->searchable(),
        Tables\Filters\TrashedFilter::make(),
      ])
      ->actions([
        Tables\Actions\EditAction::make()
          ->slideOver(),
        Tables\Actions\DeleteAction::make(),
        Tables\Actions\ForceDeleteAction::make(),
        Tables\Actions\RestoreAction::make(),
      ])
      ->bulkActions(self::bulkActions())
      // ->groups([
      //   ,
      //   ])
      ->defaultGroup(
        Tables\Grouping\Group::make('faq_category_id')
          ->titlePrefixedWithLabel(false)
          ->getTitleFromRecordUsing(fn (Model $record): string => $record->faq_category()->withoutGlobalScopes()->first()->title)
        )
      ->reorderable('order')
      ->defaultSort('order')
      ->paginatedWhileReordering();
  }

  public static function getPages(): array
  {
    return [
      'index' => Pages\ManageFaqs::route('/'),
    ];
  }

  public static function getEloquentQuery(): Builder
  {
    return parent::getEloquentQuery()
      ->withoutGlobalScopes();
  }
}
