<?php

namespace Neon\Admin\Resources;

use Camya\Filament\Forms\Components\TitleWithSlugInput;
use Filament\Forms;
use Filament\Forms\Components\Component;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\MorphToSelect;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Str;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Neon\Admin\Resources\Traits\NeonAdmin;
use Neon\Admin\Resources\FaqCategoryResource\Pages;
use Neon\Admin\Resources\FaqCategoryResource\RelationManagers;
use Neon\Attributable\Models\Attribute;
use Neon\Faq\Models\FaqCategory;
use Neon\Models\Scopes\ActiveScope;
use Neon\Models\Statuses\BasicStatus;
use Neon\Site\Models\Scopes\SiteScope;
use Neon\Site\Models\Site;

class FaqCategoryResource extends Resource
{
  use NeonAdmin;

  // protected static ?int $navigationSort = 6;

  protected static ?string $model = FaqCategory::class;

  // protected static ?string $navigationIcon = 'heroicon-o-question-mark-circle';

  // protected static ?string $activeNavigationIcon = 'heroicon-s-question-mark-circle';

  protected static ?string $recordTitleAttribute = 'title';

  public static function getNavigationParentItem(): string
  {
    return __('neon-admin::admin.navigation.faq');
  }

  public static function getNavigationLabel(): string
  {
    return __('neon-admin::admin.navigation.faq_category');
  }

  public static function getNavigationGroup(): string
  {
    return __('neon-admin::admin.navigation.web');
  }

  public static function getModelLabel(): string
  {
    return __('neon-admin::admin.models.faq_category');
  }

  public static function getPluralModelLabel(): string
  {
    return __('neon-admin::admin.models.faq_categories');
  }

  public static function items(): array
  {
    $t = [
      Select::make('site')
        ->label(__('neon-admin::admin.resources.faq_category.form.fields.site.label'))
        ->multiple()
        ->relationship(titleAttribute: 'title'),
      TitleWithSlugInput::make(
          fieldTitle: 'title',
          titleLabel: __('neon-admin::admin.resources.faq_category.form.fields.title.label'),
          fieldSlug: 'slug',
          slugLabel: __('neon-admin::admin.resources.faq_category.form.fields.slug.label'),
          urlHostVisible: false
      ),
      SpatieMediaLibraryFileUpload::make(FaqCategory::MEDIA)
        ->label(trans('neon-admin::admin.resources.faq_category.form.fields.media.label'))
        ->collection(FaqCategory::MEDIA)
        ->responsiveImages(),
      Forms\Components\RichEditor::make('lead')
        ->label(__('neon-admin::admin.resources.faq_category.form.fields.lead.label'))
        ->toolbarButtons([
          'bold',
          'italic',
          // 'bulletList',
          // 'orderedList',
          // 'blockquote',
          'redo',
          'strike',
          'underline',
          'undo',
        ]),
      Forms\Components\Section::make()
        ->columns([
          'sm' => 3,
          'xl' => 6,
          '2xl' => 9,
        ])
        ->schema([
          Select::make('status')
            ->label(__('neon-admin::admin.resources.faq_category.form.fields.status.label'))
            ->required()
            ->reactive()
            ->native(false)
            ->default(BasicStatus::default())
            ->options(BasicStatus::class)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ]),
          Forms\Components\DateTimePicker::make('published_at')
            ->label(trans('neon-admin::admin.resources.faq_category.form.fields.published_at.label'))
            ->native(false)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ]),
          Forms\Components\DateTimePicker::make('expired_at')
            ->label(trans('neon-admin::admin.resources.faq_category.form.fields.expired_at.label'))
            ->minDate(now())
            ->native(false)
            ->columnSpan([
              'sm' => 1,
              'xl' => 2,
              '2xl' => 3,
            ])
        ])

    ];

    return $t;
  }

  public static function table(Table $table): Table
  {
    return $table
      ->columns([
        Tables\Columns\TextColumn::make('title')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.title.label'))
          ->searchable(),
        Tables\Columns\TextColumn::make('site.title')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.site.label'))
          ->listWithLineBreaks()
          ->bulleted()
          ->searchable(),
        Tables\Columns\IconColumn::make('status')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.status.label'))
          ->icon(fn (BasicStatus $state): string => match ($state) {
            BasicStatus::New      => 'heroicon-o-sparkles',
            BasicStatus::Active   => 'heroicon-o-check-circle',
            BasicStatus::Inactive => 'heroicon-o-x-circle',
          })
          ->color(fn (BasicStatus $state): string => match ($state) {
            BasicStatus::New      => 'gray',
            BasicStatus::Active   => 'success',
            BasicStatus::Inactive => 'danger',
          })
          ->searchable()
          ->sortable(),
        Tables\Columns\TextColumn::make('published_at')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.published_at.label'))
          ->description(fn (FaqCategory $record): string => $record->expired_at?->format('M j, Y') ?: '-')
          ->date()
          ->sortable(),
        Tables\Columns\TextColumn::make('created_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
        Tables\Columns\TextColumn::make('updated_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
        Tables\Columns\TextColumn::make('deleted_at')
          ->dateTime()
          ->sortable()
          ->toggleable(isToggledHiddenByDefault: true),
      ])
      ->filters([
        Tables\Filters\SelectFilter::make('site')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.site.label'))
          ->native(false)
          ->relationship('site', 'title'),
        Tables\Filters\SelectFilter::make('status')
          ->label(__('neon-admin::admin.resources.faq_category.form.fields.status.label'))
          ->options(BasicStatus::class)
          ->native(false)
          ->searchable(),
        Tables\Filters\TrashedFilter::make()
          ->native(false),
      ])
      ->actions([
        Tables\Actions\EditAction::make()
          ->slideOver(),
        Tables\Actions\DeleteAction::make(),
        Tables\Actions\ForceDeleteAction::make(),
        Tables\Actions\RestoreAction::make(),
      ])
      ->bulkActions(self::bulkActions())
      ->reorderable('order')
      ->defaultSort('order')
      ->paginatedWhileReordering();
  }

  public static function getPages(): array
  {
    return [
      'index' => Pages\ManageFaqCategories::route('/'),
    ];
  }

  public static function getEloquentQuery(): Builder
  {
    return parent::getEloquentQuery()
      ->withoutGlobalScopes();
  }
}
