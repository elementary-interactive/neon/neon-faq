<?php

namespace Neon\Admin\Resources\FaqResource\Pages;

use Neon\Admin\Resources\FaqResource;
use Filament\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageFaqs extends ManageRecords
{
  protected static string $resource = FaqResource::class;

  protected function getHeaderActions(): array
  {
    return [
      Actions\CreateAction::make()
        ->slideOver(),
    ];
  }
}
