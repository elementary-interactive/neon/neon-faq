<?php

use Neon\Faq\Models\Faq;
use Spatie\Image\Enums\Fit;

return [
  'collections'   => [

    /**
     * 
     */ 
    Faq::MEDIA   => [],
    
  ],

  /** Media conversations.
   * 
   */
  'conversations' => [
    'faq-item' => [
      'fit'           => Fit::Contain,
      'height'        => 300,
      'optimize'      => true,
      'responsive'    => true,
    ]
  ]
];