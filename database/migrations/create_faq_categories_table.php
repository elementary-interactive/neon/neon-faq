<?php

use Neon\Models\Statuses\BasicStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('faq_categories', function (Blueprint $table) {
      $table->uuid('id'); // We are using UUID as primary key.

      $table->string('title');
      $table->string('slug');
      $table->text('lead')
        ->nullable()
        ->default(null);

      $table->char('status', 1)
        ->default(BasicStatus::default()->value);

      $table->timestamp('published_at')
          ->nullable()
          ->default(null);
      $table->timestamp('expired_at')
          ->nullable()
          ->default(null);
      
      $table->tinyInteger('order', false, true);

      $table->timestamps();
      $table->softDeletes();

      $table->primary('id');
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
      Schema::dropIfExists('faq_categories');
  }

};