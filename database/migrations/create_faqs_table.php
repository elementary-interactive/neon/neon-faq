<?php

use Neon\Models\Statuses\BasicStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('faqs', function (Blueprint $table) {
      $table->uuid('id'); // We are using UUID as primary key.

      $table->foreignUuid('faq_category_id');

      $table->string('question');
      $table->text('answer');

      $table->char('status', 1)
        ->default(BasicStatus::default()->value);

      $table->timestamp('published_at')
          ->nullable()
          ->default(null);
      $table->timestamp('expired_at')
          ->nullable()
          ->default(null);

      $table->tinyInteger('order', false, true);

      $table->timestamps();
      $table->softDeletes();

      $table->primary('id');
    });

    Schema::table('faqs', function (Blueprint $table) {
      $table->foreign('faq_category_id')->references('id')->on('faq_categories');
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
      Schema::dropIfExists('faqs');
  }

};