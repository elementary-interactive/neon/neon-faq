<?php

namespace Neon\Faq\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Neon\Attributable\Models\Traits\Attributable;
use Neon\Models\Traits\Uuid;
use Neon\Models\Traits\Publishable;
use Neon\Models\Traits\Statusable;
use Neon\Models\Basic as BasicModel;
use Neon\Site\Models\Traits\SiteDependencies;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Image\Enums\Fit;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class FaqCategory extends BasicModel implements HasMedia, Sortable
{
  use Attributable;
  use InteractsWithMedia;
  use LogsActivity;
  use Publishable;
  use SiteDependencies;
  use SoftDeletes;
  use SortableTrait;
  use Statusable;
  use Uuid; // N30N UUID to forget auto increment stuff.

  const MEDIA = 'media_faq_category';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'slug',
    'lead',
    'status',
    'published_at',
    'expired_at'
  ];

  protected $casts = [
    'title'         => 'string',
    'created_at'    => 'datetime',
    'updated_at'    => 'datetime',
    'deleted_at'    => 'datetime',
    'published_at'  => 'datetime',
    'expired_at'    => 'datetime'
  ];

  /** Set up sorting.
   *
   * @var array
   */
  public $sortable = [
    'order_column_name'     => 'order',
    'sort_when_creating'    => true,
    'sort_on_has_many'      => true,
  ];

  public function getActivitylogOptions(): LogOptions
  {
    return LogOptions::defaults();
  }

  public function registerMediaCollections(): void
  {
    $this->addMediaCollection(self::MEDIA)->singleFile();
  }

  public function registerMediaConversions(Media $media = null): void
  {
    foreach (config('neon-faq.conversations', []) as $converstaion => $params)
    {
      $media_conversation = $this->addMediaConversion($converstaion);

      if (array_key_exists('fit', $params) && isset($params['fit']))
      {
        $media_conversation->fit($params['fit']);
      }
      if (array_key_exists('height', $params) && isset($params['height']))
      {
        $media_conversation->fit($params['height']);
      }
      if (array_key_exists('width', $params) && isset($params['width']))
      {
        $media_conversation->fit($params['width']);
      }
      if (array_key_exists('responsive', $params) && $params['responsive'] === false)
      {
        $media_conversation->withResponsiveImages();
      }
      if (array_key_exists('optimized', $params) && $params['optimize'] === false)
      {
        $media_conversation->nonOptimized();
      }
      if (array_key_exists('queued', $params) && $params['queued'] === true)
      {
        $media_conversation->queued();
      } else {
        $media_conversation->nonQueued();
      }

      $media_conversation->performOnCollections(self::MEDIA);
    }
  }

  public function items(): HasMany
  {
    return $this->hasMany(Faq::class, 'faq_category_id', 'id');
  }
}
