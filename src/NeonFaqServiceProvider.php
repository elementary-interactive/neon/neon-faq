<?php

namespace Neon\Faq;

use Illuminate\Foundation\Console\AboutCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class NeonFaqServiceProvider extends PackageServiceProvider
{
  const VERSION = '1.0.0-alpha-5';

  public function configurePackage(Package $package): void
  {
    AboutCommand::add('N30N', 'FAQ', self::VERSION);

    $package
      ->name('neon-faq')
      ->hasConfigFile()
      // ->hasViews()
      ->hasMigration('create_faq_categories_table')
      ->hasMigration('create_faqs_table');
  }
}

?>